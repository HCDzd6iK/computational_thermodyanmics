      Program project_1
      !implicit none
      Real*8 :: av_mu, nv_mu, rel_error, grid, pi, factorial, N
      real*8 :: xmin, xmax, dx
      integer :: a, b, c, i, l
      real*8 :: x(100), f(100)
      dimension :: av_mu(4), nv_mu(4), rel_error(4), factorial(4)

      pi = 4.d+0*datan(1.d+0)
      N = 100
      xmin = 0.d+0
      xmax = 10.d+0           ! need to go infinite
      dx = (xmax - xmin)/dfloat(N-1)


      Do 10 a = 1,4
      b = (a+1)*2
      c = (b/2)+1
      factorial = 1.d+0

      Do 20 l = c, b
      factorial = l*factorial
20    continue
      av_mu(a) = factorial(a)/(2**dfloat(c/2))

10    continue




      do 30 i=1,100
      x(i)=xmin+dx*dfloat(i-1)   ! Discretize the domain
30    continue

      do i=1,100   ! Initialize the problem (alternative loop structure)
      f(i)= sqrt(2/pi)*(x**b)*exp(-(x**2)/2)
      end do

      Sum=0.   ! Inititialize the summation variable - needed

      do i=2,100        ! Add up all the trapezoid areas
      Sum=Sum+0.5*dx*(f(i)+f(i-1))
      end do


      write (*,40) av_mu(a)
40

      pause

      end program project_1
      
