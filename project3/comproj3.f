CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C Declared variables must be in the correct order in common statment!  C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      real*8 courant, deltat, deltax, deltay, xl_non
      real*8 yl_non, time, a, d, c, a1, d1, c1, t1, b1, dd1
      real*8 error, xmult, tfinal
      integer n
      parameter(nx=81) ! (nx-1)/(ny-1) = 1/0.75
      parameter(ny=61) ! ny= 1+(0.75/1)*(nx-1)

      real*8 temp1(nx,ny), temp2(nx,ny)
      real*8 x(nx), y(ny), Rx(nx),Ry(ny),tempx(nx),tempy(ny)
      real*8 ax(nx),dx(nx),cx(nx),ay(ny),dy(ny),cy(ny)
      
      common  courant, deltat, deltax, deltay, xl_non
     & yl_non, time, a, d, c, a1, d1, c1, t1, b1, dd1,
     & error, xmult, tfinal, n, temp1, temp2, x, y, Rx, Ry,
     & tempx, tempy, ax, dx, cx, ay, dy, cy

