CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C This program solves 2D heat equation with ADI scheme
C Temperature data is stored in temp1(nx,ny)
C x and y values are made only for the purpose of plotting.
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      Program project3
      include 'comproj3.f'
! boundary condition and temp at time=0
      do 1 i=1, nx
        temp1(i,1)=1.d+0
        temp2(i,1)=1.d+0
        temp1(i,ny)=3.d+0
        temp2(i,ny)=3.d+0
1     continue
      do 2 j=2, ny-1
        temp1(1,j)= 1.0d+0
        temp2(1,j)= 1.0d+0
        temp1(nx,j)= 1.0d+0
        temp2(nx,j)= 1.0d+0
2     continue
      do 3 i=2, nx-1
      do 4 j=2, ny-1
        temp1(i,j)=1.0d+0
        temp2(i,j)=1.0d+0
4     continue
3     continue
    
      call initialize
      n=0
10    n=n+1
      if(time.ge.tfinal) go to 20
      call xysweep
      time=time+deltat
      print*, n, time, tfinal, temp1(ny/2,nx/2)
      goto 10
20    continue
      call output
      stop
      end program project3
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine initialize
      include 'comproj3.f'
      time=0.d+0
      tfinal=1.d+0
      xl_non=1.d+0
      yl_non=0.75d+0
      courant = 0.25d+0       ! delta x = delta y, one courant number
      deltax = xl_non /(nx-1)
      deltay = deltax
      deltat = courant * (deltax**(2.d+0))
      do 1 i=1, nx-1
        do 2 j=1, ny-1
        x(i)=dble(float(i-1))*deltax  ! post processing x axis
        y(j)=dble(float(j-1))*deltay  ! post processing y axis
2       continue
1     continue

      return
      end
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine xysweep
      include 'comproj3.f'
      a= -courant/(2.d+0)
      d= courant+1.d+0
      c= -courant/2.d+0
      do 1 i=1,nx
        ax(i)= a
        dx(i)= d
        cx(i)= c
1     continue
       do 2 j=1,ny
        ay(j)= a
        dy(j)= d
        cy(j)= c
2     continue

      do 3 j=2, ny-1   ! central space x sweep
        do 4 i=1, nx-2  ! R: known value side
        Rx(i)=-ay(j)*temp1(i+1,j-1)+(2.-dy(j))*temp1(i+1,j)
     &  -cy(j)*temp1(i+1,j+1)
4      continue
        Rx(1)= Rx(1)-ax(1)*temp1(1,j)
        Rx(nx-2)= Rx(nx-2)-cx(nx-1)*temp1(nx,j)
        call tridiag(nx-2,ax,dx,cx,tempx,Rx)
        do 5  i=1, nx-2
        temp2(i+1,j)=tempx(i)
5      continue
3     continue

      do 31 i=2, nx-1   ! central space y sweep
        do 41 j=1, ny-2   ! R: known value side
        Ry(j)=-ax(i)*temp2(i-1,j+1)+(2.-dx(i))*temp2(i,j+1)
     &  -cx(i)*temp2(i+1,j+1)
41      continue
        Ry(1)=Ry(1)-ay(1)*temp2(i,1)
        Ry(ny-2)=Ry(ny-2)-cy(ny-1)*temp2(i,ny)
        call tridiag(ny-2,ay,dy,cy,tempy,Ry)
        do 51 j=1, ny-2
        temp1(i,j+1)=tempy(j)
51      continue
31    continue

      return
      end
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine tridiag (n,a1,d1,c1,t1,b1)
      implicit doubleprecision(a-h, o-z)
      dimension a1(10000),d1(10000),c1(10000),t1(10000),b1(10000),
     & dd1(10000)
      do 11 i=1,n
       dd1(i)=d1(i)  ! scratch array? why this thing works?
 11   continue
      do 22 i=2,n
         xmult=a1(i)/dd1(i-1)
         dd1(i)=dd1(i)-xmult*c1(i-1)
         b1(i)=b1(i)-xmult*b1(i-1)
 22   continue
      t1(n)=b1(n)/dd1(n)   ! back substitution
      do 33 i=n-1,1,-1
         t1(i)=(b1(i)-c1(i)*t1(i+1))/dd1(i)
 33   continue
      return
      end
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine output
      include 'comproj3.f'
      open(unit=50,file='Tsteady_c25_nx81ny61.txt',status='replace')
      do 59 j=1,ny-1
        do 58 i=1,nx-1
         write(50,102) x(i),y(j),temp1(i,j)   ! steady state temperature
58      continue
59    continue
      close(unit=50)
102   format(3(2x,d14.4))
      return
      end
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
