CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C This program for solving elliptic equation by iteration metnod
C Jacobi point, Gauss-Seidel point & line with SOR
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      Program project4
      include 'comproj4.f'
! boundary condition and temp at time=0
      do 1 i=1, nx
        temp1(i,1)=1.d+0
        temp2(i,1)=1.d+0
        temp1(i,ny)=3.d+0
        temp2(i,ny)=3.d+0
1     continue
      do 2 j=2, ny-1
        temp1(1,j)= 1.0d+0
        temp2(1,j)= 1.0d+0
        temp1(nx,j)= 1.0d+0
        temp2(nx,j)= 1.0d+0
2     continue
      do 3 i=2, nx-1
      do 4 j=2, ny-1
        temp1(i,j)=1.0d+0
        temp2(i,j)=1.0d+0
4     continue
3     continue
    
      call initialize
      write(6,120)
120   format('/jacobi point with SOR=1/jacobi line(x-line) with SOR=2/
     &jacobi line(y-line) with SOR=3/Gauss-Seidel point with SOR=4/
     &Gauss-Seidel line(x-line)with SOR=5/Gauss-Seidel line(y-line)
     &with SOR=6/')
      read(5,110) k
110   format(I2)
C     file(errsum.txt) generation
C     for iteration number and absolute error sum at the steady state
      n=0
      open(unit=80,file='errsum.txt',status='unknown')
10    n=n+1
      do 5 i=2,nx-1
      do 6 j=2,ny-1
      temp0(i,j)=temp1(i,j)
6     continue
5     continue
      if(k.eq.1) call pjasor  !Jacobi point with SOR
      if(k.eq.2) call ljasorx !Jacobi x-line with SOR
      if(k.eq.3) call ljasory !Jcobi y-line with SOR
      if(k.eq.4) call pgsor   !Gauss-Seidel point with SOR
      if(k.eq.5) call lgsorx  !Gauss-Seidel x-line with SOR
      if(k.eq.6) call lgsory  !Gauss-Seidel y-line with SOR
C     sum of absolute difference to check the steady state
       sum=0.0
       do 7 i=2,nx-1
      do 8 j=2,ny-1
      sum=sum+dabs(temp0(i,j)-temp1(i,j))
8     continue
7     continue
C     this for monitor output to check the convergence
      print*, n, sum
       if(sum.lt.errmax) go to 20 !errmax=1.0D-06 defined as a parameter
      write(80,155)n,sum
155   format(I9,2x,d16.6)
      goto 10
20    continue
       close(unit=80)
      call output
      stop
      end program project4
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine initialize
      include 'comproj4.f'

      xl_non=1.d+0
      yl_non=0.75d+0

      deltax = xl_non /(nx-1)
      deltay = deltax
C     beta=1.0d+0 ! beta=deltax/deltay, defined as a parameter

      
      do 1 i=1, nx
       x(i)=dble(float(i-1))*deltax  ! post processing x axis
  1     continue
  
        do 2 j=1, ny

        y(j)=dble(float(j-1))*deltay  ! post processing y axis
2       continue


      return
      end
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine pjasor ! point Jacobi with SOR
      include 'comproj4.f'
      w=1.0D+0!if w=1.0,point Jacobi(no sor), for w>1.0, it diverges!
      den=2.*(1.+beta**2.0)
      do 50 j=2,ny-1
       do 55 i=2, nx-1
       tempu(i,j)=(1-w)*temp1(i,j)+w*(1/den)*
     &(temp1(i+1,j)+temp1(i-1,j)+beta**2.0*(temp1(i,j+1)+temp1(i,j-1)))
55    continue
50    continue
      do 56 j=2,ny-1
       do 57 i=2, nx-1
       temp1(i,j)=tempu(i,j)
57    continue
56    continue
      return
      end
      
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine ljasorx !x-line Jacobi with SOR
      include 'comproj4.f'
C      beta=1.0D+0 is defined as a parameter
      w=1.0D+0 ! if w=1.0 , line-Jacobi (no sor)
      do 1 i=1,nx
        ax(i)= w*1.0D+0
        dx(i)= -2.*(1.0+beta**2)
        cx(i)= w*1.0D+0
1     continue
       do 2 j=1,ny
        ay(j)= w*beta**2
        dy(j)=(1.0-w)*(2.0+2.0*beta**2)
        cy(j)= w*beta**2
2     continue

      do 3 j=2, ny-1   ! central space x sweep
        do 4 i=1, nx-2  ! R: known value side
        Rx(i)=-ay(j)*temp1(i+1,j-1)-dy(j)*temp1(i+1,j)
     &  -cy(j)*temp1(i+1,j+1)
4      continue
        Rx(1)= Rx(1)-ax(1)*temp1(1,j)
        Rx(nx-2)= Rx(nx-2)-cx(nx-1)*temp1(nx,j)
        call tridiag(nx-2,ax,dx,cx,tempx,Rx)
        do 5  i=1, nx-2
        tempu(i+1,j)=tempx(i)
5      continue
3     continue
      do 57 j=2,ny-1
      do 58 i=2, nx-1
      temp1(i,j)=tempu(i,j)
58    continue
57    continue
      return
      end
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine ljasory !y-line Jacobi with SOR
      include 'comproj4.f'
C     beta=1.0D+0 is defined as a parameter
      w=1.0D+0 ! if w=1.0 , line-Jacobi (no sor)
      do 1 j=1,ny
        ay(j)= w*beta**2
        dy(j)= -2.*(1+beta**2)
        cy(j)= w*beta**2
1     continue
       do 2 i=1,nx
        ax(i)= w*1.0D+0
        dx(i)=(1-w)*(2.0+2.0*beta**2)
        cx(i)= w*1.0D+0
2     continue

      do 3 i=2, nx-1   ! central space y sweep
        do 4 j=1, ny-2  ! R: known value side
        Ry(j)=-ax(i)*temp1(i-1,j+1)-dx(i)*temp1(i,j+1)
     &  -cx(i)*temp1(i+1,j+1)
4      continue
        Ry(1)= Ry(1)-ay(1)*temp1(i,1)
        Ry(ny-2)= Ry(ny-2)-cy(ny-1)*temp1(i,ny)
        call tridiag(ny-2,ay,dy,cy,tempy,Ry)
        do 5  j=1, ny-2
        tempu(i,j+1)=tempy(j)
5      continue
3     continue
      do 58 j=2,ny-1
      do 59 i=2, nx-1
      temp1(i,j)=tempu(i,j)
59    continue
58    continue
       return
       end
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine pgsor !Point Gauss-Seidel with SOR
      include 'comproj4.f'
C      w=1.9116  !optimal over relaxation value
       w=1.9116  !the optimal value can be calculated in this case
       den=2.*(1.+beta**2.0)
      do 50 j=2,ny-1
       do 55 i=2, nx-1
       temp1(i,j)=(1.0-w)*temp1(i,j)+(w/den)*(temp1(i+1,j)+temp1(i-1,j)
     & +beta**2.0*(temp1(i,j+1)+temp1(i,j-1)))
55    continue
50    continue

      return
      end
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine lgsorx !x-line Gauss-Seidel with SOR
      include 'comproj4.f'
C      beta=1.0D+0  is defined as a parameter
      w=1.3D+0 ! if w=1.0 , x-line-Gauss-Seidel (no SOR)
      do 1 i=1,nx
        ax(i)= w*1.0D+0
        dx(i)= -2.*(1+beta**2)
        cx(i)= w*1.0D+0
1     continue
       do 2 j=1,ny
        ay(j)= w*beta**2
        dy(j)=(1-w)*(2.0+2.0*beta**2)
        cy(j)= w*beta**2
2     continue
      
      do 3 j=2, ny-1   ! central space x sweep
        do 4 i=1, nx-2  ! R: known value side
        Rx(i)=-ay(j)*temp1(i+1,j-1)-dy(j)*temp1(i+1,j)
     &  -cy(j)*temp1(i+1,j+1)
4      continue
        Rx(1)= Rx(1)-ax(1)*temp1(1,j)
        Rx(nx-2)= Rx(nx-2)-cx(nx-1)*temp1(nx,j)
        call tridiag(nx-2,ax,dx,cx,tempx,Rx)
        do 5  i=1, nx-2
        temp1(i+1,j)=tempx(i)
5      continue
3     continue
       return
       end
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine lgsory !y-line Gauss-Seidel with SOR
      include 'comproj4.f'
C      beta=1.0D+0 is defined as aparameter
      w=1.3D+0 ! w=1.0 , y-line-Gauss-Seidel (no SOR)
      do 11 j=1,ny
        ay(j)= w*beta**2
        dy(j)= -2.*(1+beta**2)
        cy(j)= w*beta**2
11     continue
       do 22 i=1,nx
        ax(i)= w
        dx(i)=(1-w)*(2.0+2.0*beta**2)
        cx(i)= w
22     continue

      do 33 i=2, nx-1   ! central space y sweep
        do 44 j=1, ny-2  ! R: known value side
        Ry(j)=-ax(i)*temp1(i-1,j+1)-dx(i)*temp1(i,j+1)
     &  -cx(i)*temp1(i+1,j+1)
44      continue
        Ry(1)= Ry(1)-ay(1)*temp1(i,1)
        Ry(ny-2)= Ry(ny-2)-cy(ny-1)*temp1(i,ny)
        call tridiag(ny-2,ay,dy,cy,tempy,Ry)
        do 55  j=1, ny-2
        temp1(i,j+1)=tempy(j)
55      continue
33     continue
       return
       end
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine tridiag (n,a1,d1,c1,t1,b1)
      implicit doubleprecision(a-h, o-z)
      dimension a1(10000),d1(10000),c1(10000),t1(10000),b1(10000),
     & dd1(10000)
      do 11 i=1,n
       dd1(i)=d1(i)  ! scratch array for temporary use
 11   continue
      do 22 i=2,n
         xmult=a1(i)/dd1(i-1)
         dd1(i)=dd1(i)-xmult*c1(i-1)
         b1(i)=b1(i)-xmult*b1(i-1)
 22   continue
      t1(n)=b1(n)/dd1(n)   ! back substitution
      do 33 i=n-1,1,-1
         t1(i)=(b1(i)-c1(i)*t1(i+1))/dd1(i)
 33   continue
      return
      end
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine output
      include 'comproj4.f'
      open(unit=50,file='Tsteady_c25_nx81ny61.txt',status='replace')
      do 59 j=1,ny
        do 58 i=1,nx
         write(50,102) x(i),y(j),temp1(i,j)   ! steady state temperature
58      continue
59    continue
      close(unit=50)
102   format(3(2x,d14.4))
      return
      end
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
